#!/usr/bin/guile -s
!#
(display "Content-type: text/plain")(newline)(newline)
(load "../db.scm")

(define configuration-file "./db/dictionary.sql")

(display (read-all-lines configuration-file))

(execute-file configuration-file)
