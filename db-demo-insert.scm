#!/usr/bin/guile -s
!#

(display "Content-type: text/plain\n\n")(newline)(newline)(newline)

(load "../db.scm")

(define insert-file "./db/db-demo-insert.sql")

(display (read-all-lines insert-file))

(execute-file insert-file)
